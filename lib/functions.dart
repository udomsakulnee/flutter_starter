import 'package:flutter/cupertino.dart';

// ประกาศ name parameter
void httpRequest({
  @required String url,
  String method = 'GET',
}) {}

void printPerson(
  String name, [
  int age,
  String sex = 'male',
]) {}

void main() {
//
//   bool isEven(int num){
//     return num % 2 == 0;
//   }
//
  // arrow function ใช้ได้แค่อยู่ในบรรทัดเดียวกันเท่านั้น
  bool isEven(int num) => num % 2 == 0;
  print(isEven(6));

  httpRequest(
    url: "http://www.example.com",
    method: "POST",
  );

  // required: name
  // optional: age
  // optional: sex
  printPerson("foo", 20, "female");
}
