void main() {
  // บอก type   String = key, String =  value
  Map<String, String> myMap1 = {'hello': 'world'};

  // ไม่ได้บอก type  ดู type จากค่าที่ set เข้า ปล. var เปลียนแปลง type ไม่ได้
  var myMap2 = {'say': 'hi'};
  print(myMap2['say']);
  myMap2['hello'] = 'world';
  print(myMap2['hello']);
  myMap2.length;
}


