void main() {
  String name = "Hello";
  var age = 20;
  String mix = '$name World $age';
  print(mix);

  dynamic gender = 'male';
  gender = 1;
  print(gender);

  int count;
  print(count);

  final x = 1;
  var y = const [];
  y = [1, 2, 3];
  const z = [];
}
