void main() {
  var myList = [1,2,3];
  myList.add(4);
  print(myList.length);

  var myList2 = [...myList, 5]; //space operator
  print(myList2);

  var myList3 = [
    for (var i in myList) 'myList3: $i',
    if(myList.length > 2 )'end'
  ];
  print(myList3);

  var myList4 = myList.map((item) => 'myList4: $item').toList();
  print(myList4);
}
